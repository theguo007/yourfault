package com.example.kevin.yourfault;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;
import java.util.TimeZone;

public class EarthquakeService extends Service {

    private static final int INTERVAL = 1000 * 60 * 5;
    private static final int SECOND = 1000;
    private static final String TAG = "Service";
    private static final String QUAKEID = "QUAKEID";
    private GoogleApiClient mApiClient;

    private final String URLSTRINGPREFIX = "http://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=";
    private String quakeJSON = "";
    private String picsJSON = "";
    private static final String START_ACTIVITY = "/start_activity";


    @Override
    public void onCreate() {
        super.onCreate();
        mApiClient = new GoogleApiClient.Builder( this )
                .addApi( Wearable.API )
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        /* Successfully connected */
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        /* Connection was interrupted */
                    }
                })
                .build();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        createAndStartTimer();
        return START_STICKY;
    }

    private String getFullUrl(){
        SimpleDateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        writeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar now = Calendar.getInstance();
        if(now.get(Calendar.MINUTE) >= 5){
            now.set(Calendar.MINUTE, now.get(Calendar.MINUTE) - 5);
        }
        return URLSTRINGPREFIX + writeFormat.format(now.getTime());
    }

    private void createAndStartTimer() {
        CountDownTimer timer = new CountDownTimer(INTERVAL, SECOND) {
            public void onTick(long millisUntilFinished) { }
            public void onFinish() {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        URL url;
                        try {
                            url = new URL(getFullUrl());
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                            return;
                        }
                        HttpURLConnection urlConnection = null;
                        try {
                            urlConnection = (HttpURLConnection) url.openConnection();
                            urlConnection.connect();
                            InputStream in = urlConnection.getInputStream();
                            Scanner scanner = new Scanner(in);
                            StringBuilder x = new StringBuilder();
                            while (scanner.hasNextLine()){
                                x.append(scanner.nextLine());
                            }
                            quakeJSON = x.toString();
//                            quakeJSON = quakeJSON.substring(0, quakeJSON.lastIndexOf(",")) + "]}";
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            if (urlConnection != null) {
                                urlConnection.disconnect();
                            }
                        }
                        try{
                            Log.i(TAG, quakeJSON);
                            JSONObject quakeJSON = new JSONObject(EarthquakeService.this.quakeJSON);
                            JSONArray features = quakeJSON.getJSONArray("features");
                            if (features.length() == 0){
                                Log.i(TAG, "No recent earthquake");
                            } else {
                                Log.i(TAG, "ONIX USED EARTHQUAKE!!!");

                                double mag = features.getJSONObject(0)
                                        .getJSONObject("properties")
                                        .getDouble("mag");
                                String place = features.getJSONObject(0)
                                        .getJSONObject("properties")
                                        .getString("place");
                                double lat = features.getJSONObject(0)
                                        .getJSONObject("geometry")
                                        .getJSONArray("coordinates")
                                        .getDouble(1);
                                double lng = features.getJSONObject(0)
                                        .getJSONObject("geometry")
                                        .getJSONArray("coordinates")
                                        .getDouble(0);
                                Log.i(TAG, mag + " " + place + " " + lat + " " + lng);
                                ArrayList<String> photoUrl = attachPhotos(lat, lng);
                                Earthquake newQuake = new Earthquake(mag, lat, lng, place, photoUrl);
                                EarthquakeSingleton.getInstance(getApplicationContext()).getEarthquakes().add(newQuake);
                                EarthquakeSingleton.getInstance(getApplicationContext()).saveEarthquakes();
                                mApiClient.connect();
                                sendMessage(START_ACTIVITY, place.split(" of ")[1] + "/" + mag);
                                Intent i = new Intent(getApplicationContext(), QuakeMapActivity.class);
                                i.putExtra(QUAKEID, newQuake.getId());
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);

                            }
                        } catch (JSONException e) {
                            Log.e(TAG, e + "");
                        }
                    }
                }).start();
                createAndStartTimer();
            }
        };
        timer.start();
    }

    private ArrayList<String> attachPhotos(double lat, double lng){
        URL url;
        try {
            url = new URL(getInstaUrl(lat, lng));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            InputStream in = urlConnection.getInputStream();
            Scanner scanner = new Scanner(in);
            StringBuilder x = new StringBuilder();
            while (scanner.hasNextLine()){
                x.append(scanner.nextLine());
            }
            picsJSON = x.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        try {
            ArrayList<String> urlList = new ArrayList<>();
            Log.i(TAG, picsJSON);
            JSONObject quakeJSON = new JSONObject(picsJSON);
            JSONArray dataJSON = quakeJSON.getJSONArray("data");
            for (int i = 0; i < dataJSON.length(); i++){
                if (dataJSON.getJSONObject(i).getString("type").equals("image")){
                    urlList.add(dataJSON.getJSONObject(i)
                            .getJSONObject("images")
                            .getJSONObject("standard_resolution")
                            .getString("url"));
                }
            }
            return urlList;
        } catch (JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    private String getInstaUrl(double lat, double lng){
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append("https://api.instagram.com/v1/media/search?")
                .append("lat=" + lat + "&")
                .append("lng=" + lng + "&")
                .append("client_id=ea6e161026524bd0aa977130c9472f3a&")
                .append("distance=5000");
        return urlBuilder.toString();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    private void sendMessage( final String path, final String text ) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mApiClient ).await();
                for(Node node : nodes.getNodes()) {
//                    Log.i(TAG, nodes.getNodes().size() + "");
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mApiClient, node.getId(), path, text.getBytes() ).await();
                }
            }
        }).start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mApiClient.disconnect();
    }
}