package com.example.kevin.yourfault;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

public class TestActivity extends AppCompatActivity {
    private TextView testString;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        testString = (TextView)findViewById(R.id.hello_world);
        UUID test = (UUID) getIntent().getSerializableExtra("test");
        Earthquake quake = EarthquakeSingleton.getInstance(this).getEarthquake(test);
        testString.setText(quake.get_urls().get(0));
    }

//    public String testString(){
//        SimpleDateFormat writeFormat = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss");
//        writeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
//        Date date = new Date();
//        return writeFormat.format( date );
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
