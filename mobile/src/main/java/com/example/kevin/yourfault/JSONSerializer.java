package com.example.kevin.yourfault;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

/**
 * Created by Kevin on 10/5/2015.
 */
public class JSONSerializer {
    private String mFilename;
    private Context mAppContext;

    public JSONSerializer(Context c, String file){
        mAppContext = c;
        mFilename = file;
    }

    public void saveEarthquakes(ArrayList<Earthquake> earthquakes) throws JSONException, IOException{
        JSONArray array = new JSONArray();
        for (Earthquake quake : earthquakes)
            array.put(quake.toJSON());
        Writer writer = null;
        try{
            OutputStream out = mAppContext.openFileOutput(mFilename, Context.MODE_PRIVATE);
            writer = new OutputStreamWriter(out);
            writer.write(array.toString());
        } finally {
            if (writer != null)
                writer.close();
        }
    }

    public ArrayList<Earthquake> loadEarthquakes() throws JSONException, IOException{
        ArrayList<Earthquake> earthquakes = new ArrayList<Earthquake>();
        BufferedReader reader = null;
        try{
            InputStream in = mAppContext.openFileInput(mFilename);
            reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder jsonString = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                jsonString.append(line);
            }
            JSONArray array = (JSONArray) new JSONTokener(jsonString.toString()).nextValue();
            for (int index = 0; index < array.length(); index++){
                earthquakes.add(new Earthquake(array.getJSONObject(index)));
            }
        } catch (FileNotFoundException e){

        } finally {
            if (reader != null)
                reader.close();
        }
        return earthquakes;
    }
}