package com.example.kevin.yourfault;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.UUID;

public class ImageActivity extends AppCompatActivity {

    public static final String QUAKEID = "QUAKEID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_fragment);
        FragmentManager fragmentManager = getFragmentManager();
        Fragment imageFragment = fragmentManager.findFragmentById(R.id.fragment);

        if (imageFragment == null) {
            imageFragment = ImageFragment.newInstance((UUID) getIntent().getSerializableExtra(QUAKEID));
            fragmentManager.beginTransaction()
                    .add(R.id.fragment, imageFragment)
                    .commit();
        }
    }
}