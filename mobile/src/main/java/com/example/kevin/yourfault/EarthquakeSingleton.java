package com.example.kevin.yourfault;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Kevin on 10/5/2015.
 */
public class EarthquakeSingleton {
    private static final String FILENAME = "earthquakes4.json";
    private ArrayList<Earthquake> mEarthquakes;
    private Context mAppContext;
    private JSONSerializer mSerializer;
    private static EarthquakeSingleton ourInstance;

    public static EarthquakeSingleton getInstance(Context appContext) {
        if (ourInstance == null){
            ourInstance = new EarthquakeSingleton(appContext);
        }
        return ourInstance;
    }

    private EarthquakeSingleton(Context context) {
        mAppContext = context;
        mSerializer = new JSONSerializer(mAppContext, FILENAME);
        try{
            mEarthquakes = mSerializer.loadEarthquakes();
        } catch (Exception e){
            mEarthquakes = new ArrayList<Earthquake>();
        }
        if (mEarthquakes.size() == 0) {
//            Earthquake sample = new Earthquake(10, 37.4225, -122.1653, "Stanfurd");
//            mEarthquakes.add(sample);
        }
    }

    public ArrayList<Earthquake> getEarthquakes(){
        return mEarthquakes;
    }

    public Earthquake getEarthquake(UUID id){
        for (Earthquake e : mEarthquakes) {
            if (e.getId().equals(id))
                return e;
        }
        return null;
    }

    public boolean saveEarthquakes(){
        try{
            mSerializer.saveEarthquakes(mEarthquakes);
            return true;
        } catch (Exception e){
            Log.d("Singleton", "earthquake save was unsuccessful");
            return false;
        }
    }
}
