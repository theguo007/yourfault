package com.example.kevin.yourfault;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.wearable.Wearable;

import java.util.UUID;

public class QuakeMapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private Earthquake quake;

    private GoogleApiClient mGoogleApiClient;
    public static int UPDATE_INTERVAL_MS = 1000 * 60 * 10;
    public static int FASTEST_INTERVAL_MS = 1000;
    public static final String QUAKEID = "QUAKEID";
    private TextView mLocation;
    private TextView mDistance;
    private TextView mMagnitude;
    private Button mInstaButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quake_map);
        quake = EarthquakeSingleton.getInstance(this).getEarthquake((UUID)getIntent().getSerializableExtra(QUAKEID));
        mLocation = (TextView)findViewById(R.id.map_location);
        mDistance = (TextView)findViewById(R.id.map_distance);
        mMagnitude = (TextView)findViewById(R.id.map_magnitude);
        mMagnitude.setText(quake.get_magnitude() + " magnitude");
        mLocation.setText(quake.get_name());

        mInstaButton = (Button)findViewById(R.id.map_button);
        mInstaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(QuakeMapActivity.this, ImageActivity.class);
                i.putExtra(QUAKEID, quake.getId());
                startActivity(i);
            }
        });

        try{
            LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null){
                mDistance.setText(distFrom(location.getLatitude(), location.getLongitude(), quake.get_latitude(), quake.get_longitude()) + "km away you" );
            }
        } catch (Exception e){
        }
        FragmentManager fragmentManager =  getFragmentManager();
        Fragment mapFragment = fragmentManager.findFragmentById(R.id.google_map);

        if (mapFragment == null) {
            mapFragment = new MapFragment();
            fragmentManager.beginTransaction()
                    .add(R.id.google_map, mapFragment)
                    .commit();
        }

        ((MapFragment) mapFragment).getMapAsync(this);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addApi(Wearable.API)  // used for data layer API
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public static int distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371; //kilometers
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return (int)(earthRadius * c);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(Bundle bundle) {

        LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(UPDATE_INTERVAL_MS)
                .setFastestInterval(FASTEST_INTERVAL_MS);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                    }
                });
    }

    @Override
    public void onLocationChanged(Location location){
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connResult) {
    }

    @Override
    public void onMapReady(GoogleMap map) {
//        Log.i("idk", quake.get_location().latitude + " " + quake.get_location().longitude);
        LatLng location = new LatLng(quake.get_latitude(), quake.get_longitude());
        map.setMyLocationEnabled(true);
        UiSettings settings = map.getUiSettings();
        settings.setZoomControlsEnabled(true);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 5));
        map.addMarker(new MarkerOptions()
                .title(quake.get_name())
                .snippet("")
                .position(location));
    }
}