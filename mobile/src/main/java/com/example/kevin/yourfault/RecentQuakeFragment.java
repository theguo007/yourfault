package com.example.kevin.yourfault;

import android.app.ListFragment;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.ArrayList;
import java.util.Collections;

/**
 * A placeholder fragment containing a simple view.
 */
public class RecentQuakeFragment extends ListFragment{

    public static final String QUAKEID = "QUAKEID";
    private static final String START_ACTIVITY = "/start_activity";
    private GoogleApiClient mApiClient;
    private ArrayList<Earthquake> mQuakes;
    public RecentQuakeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Wearable.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                    /* Successfully connected */
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                    /* Connection was interrupted */
                    }
                })
                .build();
        mApiClient.connect();

        mQuakes = EarthquakeSingleton.getInstance(getActivity()).getEarthquakes();
        EarthquakeAdapter adapter = new EarthquakeAdapter(mQuakes);
        setListAdapter(adapter);
        Intent i = new Intent(getActivity(), EarthquakeService.class);
        getActivity().startService(i);

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id){
        super.onListItemClick(l, v, position, id);
        Earthquake quake = ((EarthquakeAdapter)getListAdapter()).getItem(position);
        sendMessage(START_ACTIVITY, quake.get_name() + "/" + quake.get_magnitude());
        Intent i = new Intent(getActivity(), QuakeMapActivity.class);
        i.putExtra(QUAKEID, quake.getId());
        startActivity(i);
    }

    private class EarthquakeAdapter extends ArrayAdapter<Earthquake>{
        public EarthquakeAdapter(ArrayList<Earthquake> quakes){
            super(getActivity(), 0, quakes);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            if (convertView == null){
                convertView = getActivity().getLayoutInflater().inflate(R.layout.earthquake_item, null);
            }

            Earthquake quake = getItem(position);
            Log.e("RecentQuakeFragment", (quake == null) + "");
            TextView location = (TextView)convertView.findViewById(R.id.quake_location);
            location.setText(quake.get_name());
            TextView magnitude = (TextView)convertView.findViewById(R.id.quake_magnitude);
            magnitude.setText(quake.get_magnitude_string());

            return convertView;
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        ((EarthquakeAdapter)getListAdapter()).notifyDataSetChanged();
    }

    private void sendMessage( final String path, final String text ) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mApiClient ).await();
                for(Node node : nodes.getNodes()) {
                    Log.i("just work", "please work");
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                        mApiClient, node.getId(), path, text.getBytes() ).await();
                }
            }
        }).start();
    }
}