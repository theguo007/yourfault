package com.example.kevin.yourfault;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.UUID;

/**
 * A placeholder fragment containing a simple view.
 */
public class ImageFragment extends Fragment {
    private static final String TAG = "imagefragment";
    private Earthquake quake;
    private int index = 0;
    private ImageView mImage;
    private ArrayList<String> urls;

    private GoogleApiClient mApiClient;

    public static final String QUAKEID = "QUAKEID";
    public static final String SHAKE = "shake";
//    public static final String COMMENT = "comment";

    public ImageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        quake = EarthquakeSingleton.getInstance(getActivity()).getEarthquake((UUID)getActivity().getIntent().getSerializableExtra(QUAKEID));
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(getActivity());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SHAKE);
        bManager.registerReceiver(bReceiver, intentFilter);

        mApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Wearable.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                    /* Successfully connected */
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                    /* Connection was interrupted */
                    }
                })
                .build();
        mApiClient.connect();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_image, parent, false);
        urls = quake.get_urls();
        mImage = (ImageView)v.findViewById(R.id.instaButton);
        nextPic();
        return v;
    }

    private BroadcastReceiver bReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(SHAKE)) {
                nextPic();
            }
        }
    };

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    public static ImageFragment newInstance(UUID quakeID){
        Bundle args = new Bundle();
        args.putSerializable(QUAKEID, quakeID);

        ImageFragment fragment = new ImageFragment();
        fragment.setArguments(args);

        return fragment;
    }

    public void nextPic(){
        if (urls.size() == 0){
            mImage.setBackgroundResource(R.drawable.no_image_available);
        } else {
            new DownloadImageTask(mImage).execute(urls.get(index));
            if(index == urls.size() - 1){
                index = 0;
            } else {
                index++;
            }
        }
//        sendMessage(COMMENT, );
    }
}