package com.example.kevin.yourfault;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

/**
 * Created by Kevin on 10/15/2015.
 */

public class PhoneListenerService extends WearableListenerService{

    private static final String TAG = "phone listener service";
    private static final String SHAKE = "shake";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.i(TAG, "phone message received");
        if (messageEvent.getPath().equals(SHAKE)){
            Intent i = new Intent(ImageFragment.SHAKE);
            LocalBroadcastManager.getInstance(this).sendBroadcast(i);
        }
    }
}