package com.example.kevin.yourfault;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Kevin on 10/5/2015.
 */
public class Earthquake {
    private double _magnitude;
    private double _lat;
    private double _lng;
    private String _locationName;
    private UUID _mId;
    private String JSON;
    private ArrayList<String> _urls = new ArrayList<String>();

    public static final String ID = "id";
    public static final String MAGNITUDE = "magnitude";
    public static final String LAT = "lat";
    public static final String LONG = "long";
    public static final String NAME = "name";
    public static final String JSONURL = "urls";
    public Earthquake(double mag, double lat, double lng, String location, ArrayList<String> urls){
        _magnitude = mag;
        _lat = lat;
        _lng = lng;
        _locationName = location;
        _mId = UUID.randomUUID();
        _urls = urls;
    }

    public Earthquake(JSONObject json) throws JSONException{
        _mId = UUID.fromString(json.getString(ID));
        _magnitude = json.getDouble(MAGNITUDE);
        _lat = json.getDouble(LAT);
        _lng = json.getDouble(LONG);
        _locationName = json.getString(NAME);
        JSON = json.getString(JSONURL);
        JSONObject jsonObject = new JSONObject(JSON);
        JSONArray jsonArray = jsonObject.getJSONArray("data");
        for (int i = 0; i < jsonArray.length(); i++){
            if (jsonArray.getJSONObject(i).getString("type").equals("image")){
                addUrl(jsonArray.getJSONObject(i)
                        .getJSONObject("images")
                        .getJSONObject("standard_resolution")
                        .getString("url"));
            }
        }
    }

    public double get_magnitude(){
        return _magnitude;
    }

    public Double get_latitude(){
        return _lat;
    }
    public Double get_longitude() { return _lng; }

    public String get_magnitude_string(){
        return _magnitude + "M";
    }

    public String get_name(){
        return _locationName.split(" of ")[1];
    }
    public UUID getId(){
        return _mId;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(ID, _mId);
        json.put(MAGNITUDE, _magnitude);
        json.put(LAT, _lat);
        json.put(LONG, _lng);
        json.put(NAME, _locationName);
        json.put(JSONURL, JSON);
        return json;
    }

    public ArrayList<String> get_urls() {
        return _urls;
    }

    public void addUrl(String url){
        _urls.add(url);
    }

}
