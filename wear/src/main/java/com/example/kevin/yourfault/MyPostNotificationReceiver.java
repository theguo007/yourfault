package com.example.kevin.yourfault;

import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

public class MyPostNotificationReceiver extends WearableListenerService {
    private static final String START_ACTIVITY = "/start_activity";
    private static final String START_MAP = "start the map";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if( messageEvent.getPath().equalsIgnoreCase( START_ACTIVITY ) ) {
            String mEarthquake = new String(messageEvent.getData(), StandardCharsets.UTF_8);
            String[] info = mEarthquake.split("/");
            String mag = info[1];
            String location = info[0];
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, i, 0);

            int notificationId = 001;
            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.earthquake_notification_icon)
                            .setContentTitle("Earthquake!")
                            .setContentText(mag + " magnitude earthquake in " + location)
                            .addAction(R.drawable.earthquake_notification_icon, "Open App", pendingIntent);
            NotificationManagerCompat notificationManager =
                    NotificationManagerCompat.from(this);
            notificationManager.notify(notificationId, notificationBuilder.build());
        } else {
            super.onMessageReceived( messageEvent );
        }
    }
}